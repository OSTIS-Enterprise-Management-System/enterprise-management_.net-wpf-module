using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Controls;
using System.Windows.Media;

using EnterpriseManagement.Core.Entity;

namespace EnterpriseManagement.Core.UI
{
    public class TreeDefinition : ControlDefinition
    {
        private readonly Func<EnterpriseManagementEntities, IEnumerable> itemsGetter;

        public TreeDefinition(string name, ImageSource icon, Func<EnterpriseManagementEntities, IEnumerable> itemsGetter)
            : base(name, icon)
        {
            if (itemsGetter == null)
            {
                throw new ArgumentNullException("itemsGetter");
            }

            this.itemsGetter = itemsGetter;
        }

        public override void Show(GroupBox panel, bool fixedView)
        {
            base.Show(panel, fixedView);

            var treeView = new TreeView
            {
                ItemsSource = new List<string>(),
            };
            
            using (var database = new EnterpriseManagementEntities())
            {
                treeView.ItemsSource = itemsGetter(database);
            }

            panel.Content = treeView;
        }

        public IEnumerable GetItems(EnterpriseManagementEntities database)
        {
            return itemsGetter(database);
        }
    }
}