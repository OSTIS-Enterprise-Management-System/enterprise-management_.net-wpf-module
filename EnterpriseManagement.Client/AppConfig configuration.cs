﻿using System.Configuration;

namespace EnterpriseManagement.Client
{
    public class OstisConfigurationSection : ConfigurationSection
    {
        [ConfigurationProperty("hyperlink")]
        public string Hyperlink
        {
            get { return (string) this["hyperlink"]; }
            set { this["hyperlink"] = value; }
        }
    }
}
