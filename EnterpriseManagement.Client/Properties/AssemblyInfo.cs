﻿using System.Reflection;
using System.Runtime.InteropServices;
using System.Windows;

[assembly: AssemblyTitle("ИСУП клиент")]
[assembly: AssemblyDescription("Интеллектуальная система управления предприятием")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Бордушко Валерий, Бордушко Артемий, Андрей Бычко")]
[assembly: AssemblyProduct("EnterpriseManagement.Client")]
[assembly: AssemblyCopyright("Copyright © Бардушко & Бычко 2015")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]

//[assembly: NeutralResourcesLanguage("en-US", UltimateResourceFallbackLocation.Satellite)]

[assembly: ThemeInfo(ResourceDictionaryLocation.None, ResourceDictionaryLocation.SourceAssembly)]


[assembly: AssemblyVersion("0.7.0.0")]
[assembly: AssemblyFileVersion("0.7.0.0")]
