﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Forms;

namespace EnterpriseManagement.Core.UI
{
    public partial class FormattedTextDialog
    {
        public FormattedTextDialog()
        {
            InitializeComponent();
            webBrowser = (WebBrowser) windowsFormsHost.Child;
        }

        public void ShowText(string header, string text, IDictionary<string, object> parameters, Action<object> linkClicked)
        {
            ShowHtml(string.Format(@"<html><head><title>{0}</title></head><body>{1}</body></html>", header, text), parameters, linkClicked);
        }

        public void ShowHtml(string html, IDictionary<string, object> parameters, Action<object> linkClicked)
        {
            parametersDictionary = parameters;
            linkClickedHandler = linkClicked;
            webBrowser.DocumentText = html;
            webBrowser.Navigating += browserNavigating;
        }

        private void browserNavigating(object sender, WebBrowserNavigatingEventArgs webBrowserNavigatingEventArgs)
        {
            if (linkClickedHandler != null)
            {
                linkClickedHandler(parametersDictionary[webBrowserNavigatingEventArgs.Url.Fragment]);
            }
            webBrowserNavigatingEventArgs.Cancel = true;
        }

        private readonly WebBrowser webBrowser;
        private IDictionary<string, object> parametersDictionary;
        private Action<object> linkClickedHandler;

        private void dialogLoaded(object sender, RoutedEventArgs e)
        {
            if (Owner != null)
            {
                Left = Owner.Left + Owner.Width/2;
                Top = Owner.Top;
            }
        }
    }
}
