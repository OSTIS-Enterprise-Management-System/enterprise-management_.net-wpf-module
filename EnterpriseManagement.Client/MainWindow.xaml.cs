﻿using System.Collections.Generic;
using System.Configuration;
using System.Data.Objects;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Input;

using EnterpriseManagement.Core.Entity;
using EnterpriseManagement.Core.UI;

namespace EnterpriseManagement.Client
{
    public partial class MainWindow
    {
        #region Инициализация

        public MainWindow()
        {
            InitializeComponent();
        }

        private void mainWindowLoaded(object sender, RoutedEventArgs e)
        {
            using (var database = new EnterpriseManagementEntities())
            {
                var currentEnterprise = database.Enterprise.Execute(MergeOption.NoTracking).FirstOrDefault(ent => ent.IsActive);
                if (currentEnterprise == null)
                {
#if DEBUG
                    if (MessageBox.Show("Создать тестовую БД?", "Вопрос", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    {
                        EnterpriseManagementEntities.CreateTestBase(database);
                        currentEnterprise = database.Enterprise.Execute(MergeOption.NoTracking).Single(ent => ent.IsActive);
                    }
#else
                    MessageBox.Show(
                        this,
                        "В базе данных не найдено текущее предприятие (со свойством IsActive = 1).\n\n" +
                        "В настоящий момент работа с приложением невозможна.\n\n" +
                        "Выберите текущее предприятие в базе данных и перезапустите программу.",
                        "Ошибка",
                        MessageBoxButton.OK,
                        MessageBoxImage.Error);
                    tabControlMain.IsEnabled = false;
#endif
                }
                if (currentEnterprise != null)
                {
                    initDocuments(database);
                    initReports(database);
                    initRefBooks();
                }
            }
        }

        #endregion

        #region Главное меню

        private string ostisConnection;

        private void showHelpClick(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(ostisConnection))
            {
                ostisConnection = ((OstisConfigurationSection) ConfigurationManager.GetSection("enterpriseManagement/ostis")).Hyperlink;
            }
            new HelpDialog { Hyperlink = ostisConnection }.ShowDialog();
        }

        private void showAboutClick(object sender, RoutedEventArgs e)
        {
            var program = Assembly.GetExecutingAssembly();
            var attributesAuthor = program.GetCustomAttributes(typeof(AssemblyCompanyAttribute), false);
            var attributesVersion = program.GetCustomAttributes(typeof(AssemblyFileVersionAttribute), false);
            MessageBox.Show(
                string.Format(
                    "Интеллектуальная система управления предприятием\n\n" +
                    "Версия: {1}\n\n" +
                    "Авторы: {0}",
                    ((AssemblyCompanyAttribute) attributesAuthor[0]).Company,
                    ((AssemblyFileVersionAttribute) attributesVersion[0]).Version),
                "О программе",
                MessageBoxButton.OK,
                MessageBoxImage.Information);
        }

        private void exitClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        #endregion

        #region Документы

        private void initDocuments(EnterpriseManagementEntities database)
        {
            var documentTypes = new List<GridDefinition>();
            foreach (var documentType in database.DocumentType.Execute(MergeOption.NoTracking))
            {
                long typeId = documentType.ID;
                documentTypes.Add(new GridDefinition(
                    documentType.Name,
                    null,
                    db =>
                    {
                        var currentEnterprise = db.Enterprise.Execute(MergeOption.NoTracking).Single(e => e.IsActive);
                        if (!currentEnterprise.Documents.IsLoaded)
                        {
                            currentEnterprise.Documents.Load(MergeOption.NoTracking);
                        }
                        return currentEnterprise.Documents.Where(d => d.TypeID == typeId).Select(d => new DocumentView(d)).ToList();
                    },
                    DocumentViewTypeDescriptor.InitializeColumns(documentType),
                    typeId));
            }
            treeDocuments.ItemsSource = documentTypes;
        }

        private void documentClick(object sender, MouseButtonEventArgs mouseButtonEventArgs)
        {
            if (treeDocuments.SelectedItem is GridDefinition)
            {
                var controlDefinition = treeDocuments.SelectedItem as GridDefinition;
                DocumentViewTypeDescriptor.InitializeProperties(controlDefinition.Tag);
                controlDefinition.Show(panelDocuments, false);
            }
        }

        #endregion

        #region Отчёты

        private void initReports(EnterpriseManagementEntities database)
        { }

        #endregion

        #region Справочники

        private void initRefBooks()
        {
            treeRefBooks.ItemsSource = new List<ControlDefinition>
            {
                new GridDefinition(
                    "Информация о предприятии",
                    Properties.Resources.RefBookEnterpriseDetails.ToSource(),
                    db =>
                    {
                        var currentEnterprise = db.Enterprise.Execute(MergeOption.NoTracking).Single(e => e.IsActive);
                        if (!currentEnterprise.Properties.IsLoaded)
                        {
                            currentEnterprise.Properties.Load(MergeOption.NoTracking);
                        }
                        return currentEnterprise.Properties.Select(p => new { Property = p.Property.Name, Value = p.RawValue, });
                    },
                    new List<GridColumnDefinition>
                    {
                        new GridColumnDefinition("Реквизит", "Property", PropertyTypeEnum.String),
                        new GridColumnDefinition("Значение", "Value", PropertyTypeEnum.String),
                    }),
                new GridDefinition(
                    "Типы документов",
                    Properties.Resources.RefBookDocumentTypes.ToSource(),
                    db => db.DocumentType.Execute(MergeOption.NoTracking),
                    new List<GridColumnDefinition>
                    {
                        new GridColumnDefinition("Наименование", "Name", PropertyTypeEnum.String),
                    }),
                new GridDefinition(
                    "Единицы измерения",
                    Properties.Resources.RefBookUnits.ToSource(),
                    db => db.Unit.Execute(MergeOption.NoTracking),
                    new List<GridColumnDefinition>
                    {
                        new GridColumnDefinition("Полное наименование", "FullName", PropertyTypeEnum.String),
                        new GridColumnDefinition("Акроним", "ShortName", PropertyTypeEnum.String),
                    }),
                new GridDefinition(
                    "Должности",
                    Properties.Resources.RefBookPositions.ToSource(),
                    db => db.Position.Execute(MergeOption.NoTracking),
                    new List<GridColumnDefinition>
                    {
                        new GridColumnDefinition("Наименование", "Name", PropertyTypeEnum.String),
                    }),
                new GridDefinition(
                    "Сотрудники",
                    Properties.Resources.RefBookEmployees.ToSource(),
                    db =>
                    {
                        var currentEnterprise = db.Enterprise.Execute(MergeOption.NoTracking).Single(e => e.IsActive);
                        if (!currentEnterprise.Employees.IsLoaded)
                        {
                            currentEnterprise.Employees.Load(MergeOption.NoTracking);
                        }
                        return currentEnterprise.Employees.Select(e => new
                        {
                            Name = string.Format("{0} {1} {2}", e.LastName, e.FirstName, e.MiddleName),
                            e.BirthDate,
                            Position = e.Position.Name
                        });
                    },
                    new List<GridColumnDefinition>
                    {
                        new GridColumnDefinition("ФИО", "Name", PropertyTypeEnum.String),
                        new GridColumnDefinition("Возраст", "BirthDate", PropertyTypeEnum.DateTime),
                        new GridColumnDefinition("Должность", "Position", PropertyTypeEnum.String),
                    }),
                new TreeDefinition(
                    "Группы продукции",
                    Properties.Resources.RefBookProductionGroups.ToSource(),
                    db =>
                    {
                        var result = new List<ProductGroup> { db.ProductGroup.Execute(MergeOption.NoTracking).Single(g => g.ParentID == null) };
                        result[0].LoadChildren(MergeOption.NoTracking);
                        return result;
                    }),
                new GridDefinition(
                    "Вся продукция",
                    Properties.Resources.RefBookAllProducts.ToSource(),
                    db => db.Product.Execute(MergeOption.NoTracking).Select(p => new { p.Name, Group = p.Group.Name, Unit = p.Unit.ShortName }),
                    new List<GridColumnDefinition>
                    {
                        new GridColumnDefinition("Наименование", "Name", PropertyTypeEnum.String),
                        new GridColumnDefinition("Группа", "Group", PropertyTypeEnum.String),
                        new GridColumnDefinition("Ед. изм.", "Unit", PropertyTypeEnum.String),
                    }),
                new GridDefinition(
                    "Выпускаемая продукция",
                    Properties.Resources.RefBookEnterpriseProducts.ToSource(),
                    db =>
                    {
                        var currentEnterprise = db.Enterprise.Execute(MergeOption.NoTracking).Single(e => e.IsActive);
                        if (!currentEnterprise.Products.IsLoaded)
                        {
                            currentEnterprise.Products.Load(MergeOption.NoTracking);
                        }
                        return currentEnterprise.Products.Select(p => new { p.Name, Group = p.Group.Name, Unit = p.Unit.ShortName });
                    },
                    new List<GridColumnDefinition>
                    {
                        new GridColumnDefinition("Наименование", "Name", PropertyTypeEnum.String),
                        new GridColumnDefinition("Группа", "Group", PropertyTypeEnum.String),
                        new GridColumnDefinition("Ед. изм.", "Unit", PropertyTypeEnum.String),
                    }),
                new GridDefinition(
                    "Прайс-лист",
                    Properties.Resources.RefBookPriceList.ToSource(),
                    db =>
                    {
                        var currentEnterprise = db.Enterprise.Execute(MergeOption.NoTracking).Single(e => e.IsActive);
                        if (!currentEnterprise.PriceList.IsLoaded)
                        {
                            currentEnterprise.PriceList.Load(MergeOption.NoTracking);
                        }
                        return currentEnterprise.PriceList.Select(p => new { Product = p.Product.Name, Price = p.Value, IsPresent = p.Positions.Count > 0 });
                    },
                    new List<GridColumnDefinition>
                    {
                        new GridColumnDefinition("Товар", "Product", PropertyTypeEnum.String),
                        new GridColumnDefinition("Цена", "Price", PropertyTypeEnum.Decimal),
                        new GridColumnDefinition("Есть на складе", "IsPresent", PropertyTypeEnum.Boolean),
                    }),
                new GridDefinition(
                    "Складские остатки",
                    Properties.Resources.RefBookWarehouse.ToSource(),
                    db =>
                    {
                        var currentEnterprise = db.Enterprise.Execute(MergeOption.NoTracking).Single(e => e.IsActive);
                        if (!currentEnterprise.WarehouseBalance.IsLoaded)
                        {
                            currentEnterprise.WarehouseBalance.Load(MergeOption.NoTracking);
                        }
                        return currentEnterprise.WarehouseBalance.Select(b => new
                        {
                            ProductCode = b.Price.Product.ID,
                            ProductName = b.Price.Product.Name,
                            Price = b.Price.Value,
                            b.Count,
                        });
                    },
                    new List<GridColumnDefinition>
                    {
                        new GridColumnDefinition("Код товара", "ProductCode", PropertyTypeEnum.Integer),
                        new GridColumnDefinition("Наименование товара", "ProductName", PropertyTypeEnum.String),
                        new GridColumnDefinition("Цена", "Price", PropertyTypeEnum.Decimal),
                        new GridColumnDefinition("Остаток", "Count", PropertyTypeEnum.Integer),
                    }),
                new GridDefinition(
                    "Все контрагенты",
                    Properties.Resources.RefBookContractors.ToSource(),
                    db =>
                    {
                        var currentEnterprise = db.Enterprise.Execute(MergeOption.NoTracking).Single(e => e.IsActive);
                        if (!currentEnterprise.Suppliers.IsLoaded)
                        {
                            currentEnterprise.Suppliers.Load(MergeOption.NoTracking);
                        }
                        if (!currentEnterprise.Purchasers.IsLoaded)
                        {
                            currentEnterprise.Purchasers.Load(MergeOption.NoTracking);
                        }
                        var list = currentEnterprise.Suppliers.Select(c => new
                        {
                            Name = c.Properties.First().StringValue,
                            IsSupplier = true,
                            IsPurchaser = currentEnterprise.Purchasers.Any(cc => cc.ID == c.ID),
                        }).ToList();
                        list.AddRange(currentEnterprise.Purchasers.Where(c => currentEnterprise.Suppliers.All(cc => cc.ID != c.ID)).Select(c => new
                        {
                            Name = c.Properties.First().StringValue,
                            IsSupplier = false,
                            IsPurchaser = true,
                        }));
                        return list;
                    },
                    new List<GridColumnDefinition>
                    {
                        new GridColumnDefinition("Наименование", "Name", PropertyTypeEnum.String),
                        new GridColumnDefinition("Поставщик", "IsSupplier", PropertyTypeEnum.Boolean),
                        new GridColumnDefinition("Заказчик", "IsPurchaser", PropertyTypeEnum.Boolean),
                    }),
                new GridDefinition(
                    "Поставщики",
                    Properties.Resources.RefBookSuppliers.ToSource(),
                    db =>
                    {
                        var currentEnterprise = db.Enterprise.Execute(MergeOption.NoTracking).Single(e => e.IsActive);
                        if (!currentEnterprise.Suppliers.IsLoaded)
                        {
                            currentEnterprise.Suppliers.Load(MergeOption.NoTracking);
                        }
                        return currentEnterprise.Suppliers.Select(c => new { Name = c.Properties.First().StringValue });
                    },
                    new List<GridColumnDefinition>
                    {
                        new GridColumnDefinition("Наименование", "Name", PropertyTypeEnum.String),
                    }),
                new GridDefinition(
                    "Заказчики",
                    Properties.Resources.RefBookSuppliers.ToSource(),
                    db =>
                    {
                        var currentEnterprise = db.Enterprise.Execute(MergeOption.NoTracking).Single(e => e.IsActive);
                        if (!currentEnterprise.Purchasers.IsLoaded)
                        {
                            currentEnterprise.Purchasers.Load(MergeOption.NoTracking);
                        }
                        return currentEnterprise.Purchasers.Select(c => new { Name = c.Properties.First().StringValue });
                    },
                    new List<GridColumnDefinition>
                    {
                        new GridColumnDefinition("Наименование", "Name", PropertyTypeEnum.String),
                    }),
            };
        }

        private void refBookClick(object sender, MouseButtonEventArgs mouseButtonEventArgs)
        {
            if (treeRefBooks.SelectedItem is ControlDefinition)
            {
                (treeRefBooks.SelectedItem as ControlDefinition).Show(panelRefBooks, true);
            }
        }

        #endregion
    }
}
