using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Controls;
using System.Windows.Media;

using EnterpriseManagement.Core.Entity;

namespace EnterpriseManagement.Core.UI
{
    public class GridDefinition : ControlDefinition
    {
        public long Tag
        { get { return tag; } }

        public IList<GridColumnDefinition> Columns
        { get { return columns; } }

        private readonly Func<EnterpriseManagementEntities, IEnumerable> itemsGetter;
        private readonly IList<GridColumnDefinition> columns;
        private readonly long tag;

        public GridDefinition(string name, ImageSource icon, Func<EnterpriseManagementEntities, IEnumerable> itemsGetter, IEnumerable<GridColumnDefinition> columns, long tag = 0)
            : base(name, icon)
        {
            if (itemsGetter == null)
            {
                throw new ArgumentNullException("itemsGetter");
            }
            if (columns == null)
            {
                throw new ArgumentNullException("columns");
            }

            this.itemsGetter = itemsGetter;
            this.columns = new List<GridColumnDefinition>(columns);
            this.tag = tag;
        }

        public override void Show(GroupBox panel, bool fixedView)
        {
            base.Show(panel, fixedView);

            var dataGrid = new DataGrid
            {
                IsReadOnly = true,
                AutoGenerateColumns = false,
                ItemsSource = new List<string>(),
            };

            if (fixedView)
            {
                dataGrid.AutoGenerateColumns = false;
                foreach (var column in columns)
                {
                    dataGrid.Columns.Add(column.CreateColumn());
                }
            }
            else
            {
                dataGrid.AutoGenerateColumns = true;
            }

            using (var database = new EnterpriseManagementEntities())
            {
                dataGrid.ItemsSource = itemsGetter(database);   
            }

            panel.Content = dataGrid;
        }
    }
}