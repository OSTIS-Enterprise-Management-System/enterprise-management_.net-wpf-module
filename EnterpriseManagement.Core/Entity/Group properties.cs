﻿using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;

namespace EnterpriseManagement.Core.Entity
{
    partial class EnterpriseManagementEntities
    {
        public List<Property> GetProperties(ProductGroup productGroup, MergeOption mergeOption = MergeOption.NoTracking)
        {
            var properties = GetAllProductGroupProperties(productGroup.ID).ToList();
            return Property.Execute(mergeOption).Where(p => properties.Contains(p.ID)).ToList();
        }
    }
}
