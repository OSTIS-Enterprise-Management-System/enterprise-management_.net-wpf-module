﻿using System;
using System.Data.Objects;

namespace EnterpriseManagement.Core.Entity
{
    partial class PropertyValue
    {
        #region Type

        public PropertyType Type
        {
            get
            {
                if (!PropertyReference.IsLoaded)
                {
                    PropertyReference.Load(MergeOption.NoTracking);
                }
                return (Property != null) ? Property.Type : null;
            }
        }

        public PropertyTypeEnum TypeAsEnum
        {
            get
            {
                var type = Type;
                return (type != null) ? type.EnumValue : PropertyTypeEnum.Unknown;
            }
        }

        #endregion

        #region Value

        public object RawValue
        {
            get
            {
                switch (TypeAsEnum)
                {
                    case PropertyTypeEnum.Boolean:
                        if (!ValueBooleanReference.IsLoaded)
                        {
                            ValueBooleanReference.Load(MergeOption.NoTracking);
                        }
                        if (ValueBoolean != null)
                        {
                            return ValueBoolean.Value;
                        }
                        break;
                    case PropertyTypeEnum.Integer:
                        if (!ValueIntegerReference.IsLoaded)
                        {
                            ValueIntegerReference.Load(MergeOption.NoTracking);
                        }
                        if (ValueInteger != null)
                        {
                            return ValueInteger.Value;
                        }
                        break;
                    case PropertyTypeEnum.Decimal:
                        if (!ValueDecimalReference.IsLoaded)
                        {
                            ValueDecimalReference.Load(MergeOption.NoTracking);
                        }
                        if (ValueDecimal != null)
                        {
                            return ValueDecimal.Value;
                        }
                        break;
                    case PropertyTypeEnum.String:
                        if (!ValueStringReference.IsLoaded)
                        {
                            ValueStringReference.Load(MergeOption.NoTracking);
                        }
                        if (ValueString != null)
                        {
                            return ValueString.Value;
                        }
                        break;
                    case PropertyTypeEnum.DateTime:
                        if (!ValueDateTimeReference.IsLoaded)
                        {
                            ValueDateTimeReference.Load(MergeOption.NoTracking);
                        }
                        if (ValueDateTime != null)
                        {
                            return ValueDateTime.Value;
                        }
                        break;
                }
                return null;
            }
        }

        public bool BooleanValue
        {
            get
            {
                if (TypeAsEnum == PropertyTypeEnum.Boolean)
                {
                    if (!ValueBooleanReference.IsLoaded)
                    {
                        ValueBooleanReference.Load(MergeOption.NoTracking);
                    }
                    return (ValueBoolean != null) ? ValueBoolean.Value : default(bool);
                }
                else
                {
                    throw new InvalidOperationException();
                }
            }
            set
            {
                if (TypeAsEnum == PropertyTypeEnum.Boolean)
                {
                    if (!ValueBooleanReference.IsLoaded)
                    {
                        ValueBooleanReference.Load(MergeOption.OverwriteChanges);
                    }
                    if (ValueBoolean == null)
                    {
                        ValueBoolean = new PropertyValueBoolean();
                    }
                    ValueBoolean.Value = value;
                }
                else
                {
                    throw new InvalidOperationException();
                }
            }
        }

        public long IntegerValue
        {
            get
            {
                if (TypeAsEnum == PropertyTypeEnum.Integer)
                {
                    if (!ValueIntegerReference.IsLoaded)
                    {
                        ValueIntegerReference.Load(MergeOption.NoTracking);
                    }
                    return (ValueInteger != null) ? ValueInteger.Value : default(long);
                }
                else
                {
                    throw new InvalidOperationException();
                }
            }
            set
            {
                if (TypeAsEnum == PropertyTypeEnum.Integer)
                {
                    if (!ValueIntegerReference.IsLoaded)
                    {
                        ValueIntegerReference.Load(MergeOption.OverwriteChanges);
                    }
                    if (ValueInteger == null)
                    {
                        ValueInteger = new PropertyValueInteger();
                    }
                    ValueInteger.Value = value;
                }
                else
                {
                    throw new InvalidOperationException();
                }
            }
        }

        public double DecimalValue
        {
            get
            {
                if (TypeAsEnum == PropertyTypeEnum.Decimal)
                {
                    if (!ValueDecimalReference.IsLoaded)
                    {
                        ValueDecimalReference.Load(MergeOption.NoTracking);
                    }
                    return (ValueDecimal != null) ? ValueDecimal.Value : default(double);
                }
                else
                {
                    throw new InvalidOperationException();
                }
            }
            set
            {
                if (TypeAsEnum == PropertyTypeEnum.Decimal)
                {
                    if (!ValueDecimalReference.IsLoaded)
                    {
                        ValueDecimalReference.Load(MergeOption.OverwriteChanges);
                    }
                    if (ValueDecimal == null)
                    {
                        ValueDecimal = new PropertyValueDecimal();
                    }
                    ValueDecimal.Value = value;
                }
                else
                {
                    throw new InvalidOperationException();
                }
            }
        }

        public string StringValue
        {
            get
            {
                if (TypeAsEnum == PropertyTypeEnum.String)
                {
                    if (!ValueStringReference.IsLoaded)
                    {
                        ValueStringReference.Load(MergeOption.NoTracking);
                    }
                    return (ValueString != null) ? ValueString.Value : default(string);
                }
                else
                {
                    throw new InvalidOperationException();
                }
            }
            set
            {
                if (TypeAsEnum == PropertyTypeEnum.String)
                {
                    if (!ValueStringReference.IsLoaded)
                    {
                        ValueStringReference.Load(MergeOption.OverwriteChanges);
                    }
                    if (ValueString == null)
                    {
                        ValueString = new PropertyValueString();
                    }
                    ValueString.Value = value;
                }
                else
                {
                    throw new InvalidOperationException();
                }
            }
        }

        public DateTime DateTimeValue
        {
            get
            {
                if (TypeAsEnum == PropertyTypeEnum.DateTime)
                {
                    if (!ValueDateTimeReference.IsLoaded)
                    {
                        ValueDateTimeReference.Load(MergeOption.NoTracking);
                    }
                    return (ValueDateTime != null) ? ValueDateTime.Value : default(DateTime);
                }
                else
                {
                    throw new InvalidOperationException();
                }
            }
            set
            {
                if (TypeAsEnum == PropertyTypeEnum.DateTime)
                {
                    if (!ValueDateTimeReference.IsLoaded)
                    {
                        ValueDateTimeReference.Load(MergeOption.OverwriteChanges);
                    }
                    if (ValueDateTime == null)
                    {
                        ValueDateTime = new PropertyValueDateTime();
                    }
                    ValueDateTime.Value = value;
                }
                else
                {
                    throw new InvalidOperationException();
                }
            }
        }

        #endregion
    }
}
